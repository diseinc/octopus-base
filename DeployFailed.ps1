if ([string]::IsNullOrEmpty($DeployDir)) {
  $DeployDir = $env:DEPLOY_DIR -replace '\\', '/'
  if ([string]::IsNullOrEmpty($DeployDir)) {
    $DeployDir = "C:/Dise"
  }
}
if (-not (Test-Path $DeployDir)) {
  Write-Error "Deploy folder ($DeployDir) does not exist! Please correct before continuing"
}
$PackageId = $OctopusParameters["Octopus.Action.Package.PackageId"]
if ([string]::IsNullOrEmpty($SymlinkName)) {
  $SymlinkName = $PackageId.ToLower()
}
$InstallationDirectoryPath = $OctopusParameters["Octopus.Action.Package.InstallationDirectoryPath"]

# This could be a failed redeploy, which means we shouldn't attempt to prune failed release.
$f = Get-Item "$DeployDir/$SymlinkName" -Force -ErrorAction SilentlyContinue
if ($f.Exists -and [bool]($f.Attributes -band [IO.FileAttributes]::ReparsePoint)) {
  $previousPackagePath = $f.Target -replace '\\', '/'
}
# Prune node_modules
if (($previousPackagePath -ne $InstallationDirectoryPath) -and 
  (Test-Path "$InstallationDirectoryPath/package.json") -and 
  (Test-Path "$InstallationDirectoryPath/node_modules") -and
  (Get-Command "Prune-Node" -errorAction SilentlyContinue)) {
  Write-Output "Pruning failed deployment folder..."
  Prune-Node "$InstallationDirectoryPath"
}