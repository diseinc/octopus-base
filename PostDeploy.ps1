if ([string]::IsNullOrEmpty($DeployDir)) {
  $DeployDir = $env:DEPLOY_DIR -replace '\\', '/'
  if ([string]::IsNullOrEmpty($DeployDir)) {
    $DeployDir = "C:/Dise"
  }
}
if (-not (Test-Path $DeployDir)) {
  Write-Error "Deploy folder ($DeployDir) does not exist! Please correct before continuing"
}
$PackageId = $OctopusParameters["Octopus.Action.Package.PackageId"]
if ([string]::IsNullOrEmpty($SymlinkName)) {
  $SymlinkName = $PackageId.ToLower()
}
$InstallationDirectoryPath = $OctopusParameters["Octopus.Action.Package.InstallationDirectoryPath"]
try {
	$DataDir = Get-ItemPropertyValue "HKLM:\SOFTWARE\WOW6432Node\Dise\ServerCom\Installer" "DataDir"
}
catch {
	Write-Warning "Dise Server Data folder not found in registry, assuming C:\DISE Server Data..."
	$DataDir = "C:\DISE Server Data"
}

# Restart procedure: 
# 1. Stop pm2 service(s) if applicable
if ((-not [string]::IsNullOrEmpty($PM2ServiceNames)) -and (Get-Command "Stop-PM2Service" -errorAction SilentlyContinue)) {
  if (Stop-PM2Service $PM2ServiceNames) {
	  Write-Output "Service(s) `"$PM2ServiceNames`" stopped successfully."
  }
}
# 2. Since there could be an active version running with symlink pointing to it,
#    attempt to kill processes and handles that use the version we are moving from
$f = Get-Item "$DeployDir/$SymlinkName" -Force -ErrorAction SilentlyContinue
if ($f.Exists -and [bool]($f.Attributes -band [IO.FileAttributes]::ReparsePoint)) {
  if (Get-Command "Kill-Node" -errorAction SilentlyContinue) {
    Kill-Node "$DeployDir/$SymlinkName"
  }
}
# 3. Recreate symlink to point to the new version
Write-Output "Creating symlink $DeployDir/$SymlinkName -> $InstallationDirectoryPath..."
New-Item -ItemType SymbolicLink -Path "$DeployDir/$SymlinkName" -Target "$InstallationDirectoryPath" -Force | Out-Null
# 4. Start pm2 service(s) if applicable
if ((-not [string]::IsNullOrEmpty($PM2ServiceNames)) -and (Get-Command "Start-PM2Service" -errorAction SilentlyContinue)) {
  if (Start-PM2Service $PM2ServiceNames) {
	  Write-Output "Service(s) `"$PM2ServiceNames`" started successfully."
  }
}
# Create new nginx config files if they don't exist
if (Get-Command "MakeConfig" -ErrorAction SilentlyContinue) {
  $nginxTemplates = Get-ChildItem "$InstallationDirectoryPath" -Filter nginx*.inc.template
  if ($nginxTemplates.count -gt 0) {
    $changed = 0
    $nginxTemplates | ForEach-Object {
      $outFile = "$DataDir/nginx/conf/$($_.Name -replace '.template','')"
      if (Test-Path "$outFile") {
        Write-Verbose "$outFile already exists, skipping..."
      }
      elseif ((MakeConfig "$InstallationDirectoryPath/$($_.Name)" "$outFile" nginx)) {
        $changed += 1
      }
    }
    if (($changed -gt 0) -and (Get-Command "RestartAndWaitService" -ErrorAction SilentlyContinue)) {
      Write-Output "Nginx config has changed!"
      try {
        RestartAndWaitService "DISEServerNginx"
      }
      catch {
        Write-Warning "Failed to restart DISEServerNginx service! This doesn't fail the deployment but please check Nginx for any configuration errors."
      }
    } 
  }
}