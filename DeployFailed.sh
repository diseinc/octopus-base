#!/usr/bin/env bash

deployDir=$(get_octopusvariable "DeployDir")
if [ -z "$deployDir" ]; then
  deployDir=${DEPLOY_DIR:-/usr/lib/dise}
fi
if [ ! -d "$deployDir" ]; then
  echo "Deploy folder ($deployDir) does not exist! Please correct before continuing"
  exit 1
fi
readonly packageId=$(get_octopusvariable "Octopus.Action.Package.PackageId")
symlinkName=$(get_octopusvariable "SymlinkName")
if [ -z "$symlinkName" ]; then
  symlinkName=${packageId,,}
fi
readonly installationDirectoryPath=$(get_octopusvariable "Octopus.Action.Package.InstallationDirectoryPath")

# This could be a failed redeploy, which means we shouldn't attempt to prune failed release.
if [ -d "$deployDir/$symlinkName" ] && \
   [ -L "$deployDir/$symlinkName" ]; then
    # Retrieve previous linked path
    previousPackagePath=$(readlink -f "$deployDir/$symlinkName")
fi
# Prune node_modules
if [ "$previousPackagePath" != "$installationDirectoryPath" ] && \
   [ -f "$installationDirectoryPath/package.json" ] && \
   [ -d "$installationDirectoryPath/node_modules" ]; then
  rm -rf "$installationDirectoryPath/node_modules"
fi