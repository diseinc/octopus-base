#!/usr/bin/env bash

readonly packageId=$(get_octopusvariable "Octopus.Action.Package.PackageId")
readonly packageVersion=$(get_octopusvariable "Octopus.Action.Package.PackageVersion")
readonly packageType=$(get_octopusvariable "PackageType")
readonly machineId=$(get_octopusvariable "Octopus.Machine.Id")
readonly installationDirectoryPath=$(get_octopusvariable "Octopus.Action.Package.InstallationDirectoryPath")
readonly artifactSource=$(get_octopusvariable "ArtifactSource")

# The ArtifactSource variable is used to transition from using download.dise.com 
# for storing artifacts but may be extended to cover other sources.
if [[ $artifactSource == "download.dise.com" ]]; then
  repoFolder=$(get_octopusvariable "RepoFolder")
  if [ -z "$repoFolder" ]; then
    repoFolder=${packageId,,}
  fi
  readonly url="https://download.dise.com?folder=$repoFolder&type=$packageType&machine=$machineId&package=$packageId&version=$packageVersion"
  tempFile=$(mktemp)
  echo "Downloading package from $url..."
  if [[ $packageType == "src" ]]; then
    wget --header="Authorization: Bearer $repoApiKey" "$url" -O $tempFile -q 2>&1
  else
    wget "$url" -O $tempFile -q 2>&1
  fi
  if [ $? -ne 0 ]; then
    echo "File was not downloaded successfully"
    exit 1
  fi

  echo "Extracting package to $installationDirectoryPath..."
  (7z x -y -aoa -o"$installationDirectoryPath" -bb0 -bd -bso0 $tempFile) || exitCode=$?
  if [[ $exitCode == 2 ]]; then
    echo "Error extracting files"
    exit 1
  fi

  [ -f $tempFile ] && rm $tempFile
fi
# NPM install
if [ -f "$installationDirectoryPath/package.json" ]; then
  readonly nodePath=$(get_octopusvariable "NodePath")
  if [ -n "$nodePath" ]; then
    PATH="$nodePath:$PATH"
  fi
  pushd "$installationDirectoryPath"
  if [ ! -f ".npmrc" ]; then
    npmToken=$(get_octopusvariable "NpmToken")
    if [ -z "$npmToken" ]; then
      npmToken=$NPM_TOKEN
    fi
    if [ -n "$npmToken" ]; then
      echo "//registry.npmjs.org/:_authToken=$npmToken\nregistry=https://registry.npmjs.org" > .npmrc
    else 
      echo "No NPM token found, npm install might fail!"
    fi
  fi
  echo "Node version: $(node -v)"
  echo "Running npm install..."
  echo "##octopus[stdout-verbose]"
  echo "##octopus[stderr-progress]"
  if [[ $packageType == "src" ]]; then
    npm install --quiet
  else
    npm install --quiet --omit=dev
  fi
  echo "##octopus[stdout-default]"
  echo "##octopus[stderr-default]"
  popd
fi
# Create node config file if it doesn't already exist
if [ -f "$installationDirectoryPath/config/default.json" ]; then
  echo "default.json already exists, skipping..."
else
  readonly defaultJsonTemplate=$(get_octopusvariable "DefaultJsonTemplate")
  if [[ -n "$defaultJsonTemplate" && -d "$installationDirectoryPath/config" ]]; then
    echo "$defaultJsonTemplate" | tee "$installationDirectoryPath/config/default.json.template"
  fi
  if [ -f "$installationDirectoryPath/config/default.json.template" ]; then 
    # should/could do envsubst, but this is better than nothing
    cp "$installationDirectoryPath/config/default.json.template" "$installationDirectoryPath/config/default.json"
  fi
fi