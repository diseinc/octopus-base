$PackageId = $OctopusParameters["Octopus.Action.Package.PackageId"]
$PackageVersion = $OctopusParameters["Octopus.Action.Package.PackageVersion"]
$MachineId = $OctopusParameters["Octopus.Machine.Id"]
$InstallationDirectoryPath = $OctopusParameters["Octopus.Action.Package.InstallationDirectoryPath"]

# The ArtifactSource variable is used to transition from using download.dise.com 
# for storing artifacts but may be extended to cover other sources.
switch ($ArtifactSource) {
  "download.dise.com" {
    if ([string]::IsNullOrEmpty($RepoFolder)) {
      $RepoFolder = $PackageId.ToLower()
    }
    $url = "https://download.dise.com?folder={0}&type={1}&machine={2}&package={3}&version={4}" -f $RepoFolder, $PackageType, $MachineId, $PackageId, $PackageVersion
    try {
      $request = [System.Net.WebRequest]::Create($url)
      if ($PackageType -eq "src") {
        $request.Headers["Authorization"] = "Bearer $RepoApiKey"
      }
      $request.AllowAutoRedirect = $false
      $response = $request.GetResponse()
      if ($response.StatusCode -eq "Found") {
        $location = $response.GetResponseHeader("Location")
      }
      else {
        $location = $url
      }
      $response.Dispose();
      $filename = [System.IO.Path]::GetFileName($location)
      $tempFile = Join-Path ([System.IO.Path]::GetTempPath()) $filename
  
      $wc = New-Object System.Net.WebClient
      if ($PackageType -eq "src") {
        $wc.Headers["Authorization"] = "Bearer $RepoApiKey"
      }
      Write-Output "Downloading package from $url..."
      $wc.DownloadFile($url, $tempFile) | Out-Null
      if (Test-Path $tempFile) {
        $ext = [System.IO.Path]::GetExtension($tempFile)
        switch ($ext) {
          { $_ -in ".7z", ".zip" } {
            Write-Output "Extracting package contents to $InstallationDirectoryPath..."
            & 7z x -y -aoa -o"$InstallationDirectoryPath" "$tempFile" | Out-Null
            if ($LastExitCode -ne 0) {
              throw "Error extracting files"
            }
          }
          { $_ -in ".exe", ".msi" } {
            Copy-Item -Path $tempFile -Destination "$InstallationDirectoryPath" -Force | Out-Null
          }
          default {
            throw "Unsupported file extension ($_)"
          }
        }
      }
      else {
        throw "File was not downloaded successfully"
      }
    }
    catch [Net.WebException] {
      Write-Error "Download failure, inner exception: $($_.Exception.ToString())"
    }
    catch {
      Write-Error $_
    }
    finally {
      if (-not ([string]::IsNullOrEmpty($tempFile)) -and (Test-Path -Path $tempFile -PathType leaf)) {
        Remove-Item "$tempFile" -Force
      }
    }
  }
  default {
  }
}
# NPM install
if (Test-Path "$InstallationDirectoryPath/package.json") {
  if (-not (Get-Command "node" -ErrorAction SilentlyContinue)) {
    if ($IsWindows -or $env:OS) {
      Write-Verbose "Falling back on Dise provided Node.js..."
      $NodePath = "${env:ProgramFiles(x86)}\DISE\DISE Server\node12"
    }
    else {
      Write-Error "No valid Node.js installation found!"
    }
  }
  if (-not [string]::IsNullOrEmpty($NodePath)) {
    # allow for specifying NodePath with environment variables, such as ${env:ProgramFiles(x86)}
    $NodePath = $ExecutionContext.InvokeCommand.ExpandString($NodePath)
    if (Test-Path "$NodePath") {
      $env:PATH = "$NodePath;$($env:PATH)"
    }
    else {
      Write-Warning "NodePath was specified but not found!"
    }
  }
  Push-Location "$InstallationDirectoryPath"
  if (-not (Test-Path ".npmrc")) {
    if ([string]::IsNullOrEmpty($NpmToken)) {
      $NpmToken = $env:NPM_TOKEN
    }
    if (-not ([string]::IsNullOrEmpty($NpmToken))) {
      [System.IO.File]::WriteAllLines(".npmrc", "//registry.npmjs.org/:_authToken=$NpmToken`nregistry=https://registry.npmjs.org")
    }
    else {
      Write-Warning "No NPM token found, npm install might fail!"
    }
  }
  Write-Output "Node.js version: $(node -v)"
  Write-Output "Running npm install..."
  Write-Output "##octopus[stdout-verbose]"
  Write-Output "##octopus[stderr-progress]"
  if ($PackageType -eq "src") {
    npm install --quiet --scripts-prepend-node-path
  }
  else {
    npm install --quiet --omit=dev --scripts-prepend-node-path
  }
  Write-Output "##octopus[stdout-default]"
  Write-Output "##octopus[stderr-default]"
  Pop-Location
}
# Create node config file if it doesn't already exist
if (Get-Command "MakeConfig" -errorAction SilentlyContinue) {
  if (Test-Path "$InstallationDirectoryPath/config/default.json") {
    Write-Verbose "default.json already exists, skipping..."
  }
  else {
    if (-not ([string]::IsNullOrEmpty($DefaultJsonTemplate))) {
      Write-Output "default.json.template:`n$DefaultJsonTemplate"
      New-Item -ItemType Directory -Force -Path "$InstallationDirectoryPath/config"
      [System.IO.File]::WriteAllLines("$InstallationDirectoryPath/config/default.json.template", $DefaultJsonTemplate)
    }
    if (Test-Path "$InstallationDirectoryPath/config/default.json.template") {
      MakeConfig "$InstallationDirectoryPath/config/default.json.template" "$InstallationDirectoryPath/config/default.json" | Out-Null
    }
    if (Test-Path "$InstallationDirectoryPath/config/default.json") {
      Write-Output "default.json:`n$([System.IO.File]::ReadAllText("$InstallationDirectoryPath/config/default.json"))"
    }
  }
}