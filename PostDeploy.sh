#!/usr/bin/env bash

deployDir=$(get_octopusvariable "DeployDir")
if [ -z "$deployDir" ]; then
  deployDir=${DEPLOY_DIR:-/usr/lib/dise}
fi
if [ ! -d "$deployDir" ]; then
  echo "Deploy folder ($deployDir) does not exist! Please correct before continuing"
  exit 1
fi
readonly packageId=$(get_octopusvariable "Octopus.Action.Package.PackageId")
symlinkName=$(get_octopusvariable "SymlinkName")
if [ -z "$symlinkName" ]; then
  symlinkName=${packageId,,}
fi
readonly installationDirectoryPath=$(get_octopusvariable "Octopus.Action.Package.InstallationDirectoryPath")

# Recreate symlink
echo "Creating symlink $deployDir/$symlinkName -> $installationDirectoryPath..."
ln -sfn "$installationDirectoryPath" "$deployDir/$symlinkName"